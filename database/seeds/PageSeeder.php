<?php

use App\Models\Action;
use App\Models\Device;
use Backpack\PageManager\app\Models\Page;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->delete();

        $datas = [
            [
                'template' => 'home',
                'name' => 'home',
                'title' => 'Home',
                'slug' => 'home',
                'content' => 'text',
                'extras' => []
            ],
        ];

        foreach( $datas as $data )
        {
            $page = new Page();
            $page->template = $data['template'];
            $page->name = $data['name'];
            $page->title = $data['title'];
            $page->slug = Str::slug($data['title']);
            $page->content = $data['content'];
            $page->extras = json_encode($data['extras']);
            $page->save();
        }
    }
}
