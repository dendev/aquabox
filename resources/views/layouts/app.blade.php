<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ucfirst(env('APP_NAME'))}} - @yield('title')</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="css/app.css">
    @stack('styles')
    </head>
<body>

@include('layouts.partials.menu', ['plants' => $plants])

<div class="container">
    @yield('content')
</div>

<script src="js/app.js"></script>
@stack('scripts')
</body>
</html>
