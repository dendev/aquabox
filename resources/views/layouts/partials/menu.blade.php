<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="#">AquaBox</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>


    <div class="collapse navbar-collapse" id="navbar-menu">
        <ul class="navbar-nav ml-auto">

            <!-- residents -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownResidentsMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Residents</a>
                <div class="dropdown-menu" aria-labelledby="dropdownResidentsMenu">
                    @foreach($plants as $plant)
                        <a class="dropdown-item" href="#">{{$page->title}}</a>
                    @endforeach
                </div>
            </li>

            <!-- plants -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownPlantsMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Plantes</a>
                <div class="dropdown-menu" aria-labelledby="dropdownPlantsMenu">
                    @foreach($plants as $plant)
                        <a class="dropdown-item" href="#">{{$page->title}}</a>
                    @endforeach
                </div>
            </li>

            <!-- services -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownServicesMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services</a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownServicesMenu">
                    @foreach($plants as $plant)
                        <a class="dropdown-item" href="#">{{$page->title}}</a>
                    @endforeach
                </div>
            </li>

        </ul>
    </div>
</nav>
