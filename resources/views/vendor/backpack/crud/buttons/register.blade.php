@if ($crud->hasAccess('show'))
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#registerModal">
        <i class="fa fa-upload"></i> Ajout
    </button>
@endif

<!-- Modal -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <form action="{{route('device_register')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="registerModalLabel">Ajout d'une box</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info" role="alert">Ajouter l'url d'enregistrement de la box</div>
                    <input type="text" name="register_url" id="device-register-url" placeholder="http://aquarest1/register" required/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <input type="submit" class="btn btn-primary">
                </div>
        </form>
    </div>
</div>
</div>
