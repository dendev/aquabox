{{--@if ($crud->hasAccess('create'))--}}
    @javascript('api_token', backpack_user()->api_token)
    <a href="javascript:void(0)" onclick="runEntry(this)" class="btn btn-sm btn-link" data-route="{{route('actions.do', $entry->getKey())}}" data-button-type="run">
        <i class="las la-play"></i> Run
    </a>
{{--@endif--}}

{{-- Button Javascript --}}
{{-- - used right away in AJAX operations (ex: List) --}}
{{-- - pushed to the end of the page, after jQuery is loaded, for non-AJAX operations (ex: Show) --}}
@push('after_scripts') @if (request()->ajax()) @endpush @endif
<script>
    if (typeof runEntry != 'function')
    {
        $("[data-button-type=run]").unbind('click');

        function runEntry(button)
        {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');
            var api_token = window.api_token;
            console.log( 'TOKEN AP');
            console.log( api_token);

            var headers = {'Authorization':`Bearer ${api_token}`};

            $.ajax({
                url: route,
                type: 'GET',
                headers: headers,
                success: function(result) {
                    console.log( result.data);
                    var msg = result.data.msg;
                    // Show an alert with the result
                    new Noty({
                        type: "success",
                        text: `Action correctement effectuée.<br><strong>${msg}</strong>`
                    }).show();

                    // Hide the modal, if any
                    $('.modal').modal('hide');

                    if (typeof crud !== 'undefined') {
                        crud.table.ajax.reload();
                    }
                },
                error: function(result) {
                    // Show an alert with the result
                    new Noty({
                        type: "warning",
                        text: "Erreur lors de l'execution de l'action"
                    }).show();
                }
            });
        }
    }

    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('cloneEntry');
</script>
@if (!request()->ajax()) @endpush @endif

