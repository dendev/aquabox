@javascript('api_token', backpack_user()->api_token)

<table>
    <thead>
        <tr>
            <th>Concerne</th>
            <th>Description</td>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
@foreach ($crud->entry->actions as $action)
    <tr>
        <td>
            {{$action->resource_label}}
        </td>
        <td>
            {{$action->description}}
        </td>
        <td>
            <a href="javascript:void(0)" onclick="runEntry(this)" class="btn btn-primary btn-list" data-route="{{route('actions.do', $action->id)}}" data-button-type="run"
                    style="width: 100%"
                    @if( $action->description) data-toggle="tooltip" data-placement="bottom" title="{{$action->description}}"@endif>
                {{$action->label}}
            </a>
        </td>
    </tr>
@endforeach
    </tbody>
</table>


{{-- Button Javascript --}}
{{-- - used right away in AJAX operations (ex: List) --}}
{{-- - pushed to the end of the page, after jQuery is loaded, for non-AJAX operations (ex: Show) --}}
@push('after_scripts') @if (request()->ajax()) @endpush @endif
<script>
    if (typeof runEntry != 'function')
    {
        $("[data-button-type=run]").unbind('click');

        function runEntry(button)
        {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');
            var api_token = window.api_token;

            var headers = {'Authorization':`Bearer ${api_token}`};

            $.ajax({
                url: route,
                type: 'GET',
                headers: headers,
                success: function(result) {
                    console.log( result);
                    var msg = result.data.msg;
                    // Show an alert with the result
                    new Noty({
                        type: "success",
                        text: `Action correctement effectuée.<br><strong>${msg}</strong>`
                    }).show();

                    // Hide the modal, if any
                    $('.modal').modal('hide');

                    if (typeof crud !== 'undefined') {
                        crud.table.ajax.reload();
                    }
                },
                error: function(result) {
                    // Show an alert with the result
                    new Noty({
                        type: "warning",
                        text: "Erreur lors de l'execution de l'action"
                    }).show();
                }
            });
        }
    }

    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('cloneEntry');
</script>
@if (!request()->ajax()) @endpush @endif
