<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-group"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}">Users</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}">Roles</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}">Permissions</a></li>
    </ul>
</li>
<hr>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('post') }}'><i class='nav-icon la la-newspaper'></i> <span>Posts</span></a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('page') }}'><i class='nav-icon la la-file-o'></i> <span>Pages</span></a></li>
<hr>


<li class='nav-item'><a class='nav-link' href='{{ backpack_url('cron') }}'><i class='nav-icon la la-calendar-times'></i> Crons</a></li>
<hr>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('device') }}'><i class='nav-icon la la-cog'></i> Devices</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('action') }}'><i class='nav-icon la la-plug'></i> Actions</a></li>
<hr>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('log') }}'><i class='nav-icon la la-terminal'></i> Logs</a></li>
