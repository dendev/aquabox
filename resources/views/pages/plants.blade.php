@extends('layouts.app')

@section('title', 'TEST')

@section('content')
    <div class="card border-primary mb-5 mt-5">
        <div class="card-body">
            <!-- title -->
            <h4 class="card-title">{{$page->title}}</h4>
            <h6 class="card-subtitle text-muted">{{$page->origin}}</h6>

            <!-- img -->
            <img class="mt-4" style="height: 200px; width: 100%; display: block;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22318%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20318%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_158bd1d28ef%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A16pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_158bd1d28ef%22%3E%3Crect%20width%3D%22318%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22129.359375%22%20y%3D%2297.35%22%3EImage%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image">

            <!-- generals -->
            <p class="card-text mt-4">{{$page->content}}</p>
            <ul class="list-group">
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Taille
                    <span class="badge badge-primary badge-pill">{{$page->height_min}} - {{$page->height_max}}</span>
                </li>
            </ul>

            <!-- needs -->
            <h6 class="mt-4">Besoins</h6>
            <ul class="list-group">
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Lumière
                    <span class="badge badge-primary badge-pill">{{$page->light}}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Substrat
                    <span class="badge badge-primary badge-pill">{{$page->substrate}}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Température
                    <span class="badge badge-primary badge-pill">{{$page->temperature_min}} - {{$page->temperature_max}}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    PH
                    <span class="badge badge-primary badge-pill">{{$page->ph_min}} - {{$page->ph_max}}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    GH
                    <span class="badge badge-primary badge-pill">{{$page->gh_min}} - {{$page->gh_max}}</span>
                </li>
            </ul>
        </div>
    </div>
@endsection
