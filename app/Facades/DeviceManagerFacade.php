<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class DeviceManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'device_manager';
    }
}
