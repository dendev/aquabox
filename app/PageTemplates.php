<?php

namespace App;

trait PageTemplates
{
    /*
    |--------------------------------------------------------------------------
    | Page Templates for Backpack\PageManager
    |--------------------------------------------------------------------------
    |
    | Each page template has its own method, that define what fields should show up using the Backpack\CRUD API.
    | Use snake_case for naming and PageManager will make sure it looks pretty in the create/update form
    | template dropdown.
    |
    | Any fields defined here will show up after the standard page fields:
    | - select template
    | - page name (only seen by admins)
    | - page title
    | - page slug
    */

    private function home()
    {
    }

    private function news()
    {
        $this->crud->addField([
            'name' => 'content',
            'label' => trans('backpack::pagemanager.content'),
            'type' => 'wysiwyg',
            'placeholder' => trans('backpack::pagemanager.content_placeholder'),
        ]);

        $this->crud->addField([   // repeatable
            'name'  => 'images',
            'label' => 'Images',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'image',
                    'type'    => 'image',
                    'label'   => 'Image',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ],
            'new_item_label'  => 'Ajouter une image', // customize the text of the button
        ]);
    }

    private function plants()
    {
        $this->crud->addField([   // CustomHTML
            'name' => 'generality_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>Généralités</h2><hr>',
        ]);

        $this->crud->addField([
            'name' => 'meaning',
            'label' => 'Signification',
            'type' => 'text',
            'fake' => true,
            'store_in' => 'extras',
        ]);

        $this->crud->addField([
            'name' => 'origin',
            'label' => 'Origine',
            'type' => 'text',
            'fake' => true,
            'store_in' => 'extras',
        ]);

        $this->crud->addField([
            'name' => 'height_min',
            'label' => 'Taille minimal',
            'type' => 'number',
            'fake' => true,
            'store_in' => 'extras',
        ]);

        $this->crud->addField([
            'name' => 'height_max',
            'label' => 'Taille maximale',
            'type' => 'number',
            'fake' => true,
            'store_in' => 'extras',
        ]);

        $this->crud->addField([
            'name' => 'content',
            'label' => 'Description',
            'type' => 'textarea',
        ]);

        $this->crud->addField([   // CustomHTML
            'name' => 'needs_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>Besoins</h2><hr>',
        ]);

        $this->crud->addField([
            'name'        => 'light',
            'label'       => "Lumiere",
            'type'        => 'select2_from_array',
            'options'     => ['low' => 'Faible', 'normal' => 'Normal', 'high' => 'Forte'],
            'allows_null' => false,
            'default'     => 'normal',
            'fake' => true,
            'store_in' => 'extras',
        ]);

        $this->crud->addField([
            'name' => 'substrate',
            'label' => 'Substrat',
            'type'        => 'select2_from_array',
            'options'     => ['epiphyte' => 'Epiphyte', 'low' => 'Pauvre', 'normal' => 'Normal', 'high' => 'Riche'],
            'allows_null' => false,
            'default'     => 'normal',
            'fake' => true,
            'store_in' => 'extras',
        ]);

        $this->crud->addField([
            'name' => 'temperature_min',
            'label' => 'Température minimal',
            'type' => 'text',
            'fake' => true,
            'store_in' => 'extras',
        ]);

        $this->crud->addField([
            'name' => 'temperature_max',
            'label' => 'Température maximal',
            'type' => 'text',
            'fake' => true,
            'store_in' => 'extras',
        ]);

        $this->crud->addField([
            'name' => 'ph_min',
            'label' => 'PH minimal',
            'type' => 'text',
            'fake' => true,
            'store_in' => 'extras',
        ]);

        $this->crud->addField([
            'name' => 'ph_max',
            'label' => 'PH maximal',
            'type' => 'text',
            'fake' => true,
            'store_in' => 'extras',
        ]);

        $this->crud->addField([
            'name' => 'gh_min',
            'label' => 'GH minimal',
            'type' => 'text',
            'fake' => true,
            'store_in' => 'extras',
        ]);

        $this->crud->addField([
            'name' => 'gh_max',
            'label' => 'GH maximal',
            'type' => 'text',
            'fake' => true,
            'store_in' => 'extras',
        ]);
    }

    private function services()
    {
        $this->crud->addField([   // CustomHTML
            'name' => 'metas_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>'.trans('backpack::pagemanager.metas').'</h2><hr>',
        ]);
        $this->crud->addField([
            'name' => 'meta_title',
            'label' => trans('backpack::pagemanager.meta_title'),
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'meta_description',
            'label' => trans('backpack::pagemanager.meta_description'),
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'meta_keywords',
            'type' => 'textarea',
            'label' => trans('backpack::pagemanager.meta_keywords'),
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([   // CustomHTML
            'name' => 'content_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>'.trans('backpack::pagemanager.content').'</h2><hr>',
        ]);
        $this->crud->addField([
            'name' => 'content',
            'label' => trans('backpack::pagemanager.content'),
            'type' => 'wysiwyg',
            'placeholder' => trans('backpack::pagemanager.content_placeholder'),
        ]);
    }
}
