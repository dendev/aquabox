<?php

namespace App\Libs;
use Illuminate\Support\Facades\Http;


class AquapyboxApi
{
    public static function do_action($action_url)
    {
        $response = self::_get($action_url);
        return $response;
    }

    public static function get_register($url)
    {
        return self::_get($url);
    }

    private static function _get($url)
    {
        $datas = false;

        $response = Http::get($url);
        if( $response->successful() )
        {
            $datas =  $response->json();
        }
        else
        {
            \Log::error("[AquarestApi:_get] AAg01: erreur request get sur ", [
            ]);
        }

        return $datas;
    }
}
