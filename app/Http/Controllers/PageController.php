<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Backpack\PageManager\app\Models\Page;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index($slug, $subs = null)
    {
        // get page
        if( ! is_null($slug) )
        {
            $page = Page::findBySlug($slug);
        }
        else
        {
            $page = Page::where('template', 'home')->first();
            $news = Page::where('template', 'news')->get();

            $this->data['posts'] = $news;
            //$this->data['posts'] = Post::all();
        }

        // redirect if not found
        if (!$page && ! is_null($slug) )
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');

        // add general datas
        //$this->data['title'] = $page->title;
        $this->data['page'] = $page->withFakes();


        // render
        return view('pages.'.$page->template, $this->data);
    }
}
