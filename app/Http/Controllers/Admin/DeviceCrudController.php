<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DeviceRequest;
use App\Libs\AquapyboxApi;
use App\Models\Action;
use App\Models\Device;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class DeviceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DeviceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {store as traitStore;}
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {update as traitUpdate;}
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Device::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/device');
        CRUD::setEntityNameStrings('device', 'devices');

        CRUD::denyAccess(['create', 'update']);
        CRUD::allowAccess(['list', 'show', 'delete']);

        CRUD::addButtonFromView('top', 'Ajout', 'register', 'end');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        CRUD::addColumn([
            'label' => 'Label',
            'name' => 'label',
            'type' => 'text'
        ]);

        CRUD::addColumn([
            'label' => 'Description',
            'name' => 'description',
            'type' => 'text'
        ]);
    }

    protected function setupShowOperation()
    {
        CRUD::set('show.setFromDb', false);

        CRUD::addColumn([
            'label' => 'Label',
            'name' => 'label',
            'type' => 'text'
        ]);

        CRUD::addColumn([
            'label' => 'Description',
            'name' => 'description',
            'type' => 'text'
        ]);

        CRUD::addColumn([
            'label'     => 'Image', // Table column heading
            'name'      => 'image_url', // The db column name
            'type'      => 'image',
            'height' => '300px',
            'width'  => '300px',
        ]);

        CRUD::addColumn([
            'label' => 'Actions',
            'name' => 'actions',
            'type' => 'actions'
        ]);

        /*
        CRUD::addColumn([
            'label' => 'Actions',
            'name' => 'actions',
            'type' => 'closure',
            'function' => function($entry) {
                return $entry->actions[0];
            }
        ]);
        */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation() // not used
    {
        CRUD::setValidation(DeviceRequest::class);

        CRUD::addField([
            'label' => 'Label',
            'name' => 'label',
            'type' => 'text'
        ]);

        CRUD::addField([
            'label' => 'Description',
            'name' => 'description',
            'type' => 'textarea'
        ]);

        CRUD::addField([   // repeatable
            'name' => 'actions',
            'label' => 'Action',
            'type' => 'repeatable',
            'fields' => [
                [
                    'label' => 'Label',
                    'name' => 'label',
                    'type' => 'text',
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],

                [
                    'label' => 'Commande',
                    'name' => 'cmd',
                    'type' => 'text',
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'label' => 'Description',
                    'name' => 'description',
                    'type' => 'textarea',
                    'wrapper' => ['class' => 'form-group col-md-12'],
                ],
            ],

            // optional
            'new_item_label' => 'Ajouter une action', // customize the text of the button
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    //
    public function register(Request $request)
    {
        $ok = false;

        $register_url = $request->input('register_url');

        $datas = AquapyboxApi::get_register($register_url);
        if (!$datas)
        {
            \Log::error("[DeviceCrudController:register] DCCr01: Error bad url", [
                'register_url' => $register_url,
                'datas' => $datas
            ]);
        }
        else
        {
            $base_url = str_replace('register', '', $register_url);
            $ok = \DeviceManager::create($datas, $base_url);
        }

        if ($ok)
            \Alert::add('success', 'Ajout correctement fait.')->flash();
        else
            \Alert::add('error', "Erreur lors de l'ajout")->flash();

        // redirect
        return redirect(route('device.index'));
    }
}
