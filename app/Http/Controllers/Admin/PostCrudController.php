<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostRequest;
use App\Http\Requests\PostRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Auth;

/**
 * Class PostCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PostCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as storeTrait;}
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { edit as editTrait; update as updateTrait;}
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Post::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/post');
        CRUD::setEntityNameStrings('post', 'posts');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'title',
            'label' => 'Titre',
            'type' => 'text',
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PostRequest::class);
        $this->_form();
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(UpdateRequest::class);
        $this->_form();
    }

    // overload
    public function update()
    {
        // check perms
        $this->crud->hasAccessOrFail('update');
        $request = $this->crud->validateRequest();

        // save photos
        $values = $this->crud->getStrippedSaveRequest();
        if( array_key_exists('photos', $values))
        {
            $photos = $values['photos'];

            $model_id = $request->get($this->crud->model->getKeyName());
            $model = $this->crud->model->find($model_id);
            $model->photos = $photos;

        }

        //
        $response = $this->updateTrait();
        return $response;

        /*
        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();
        //dd( $request->getRequestUri()); // last eleemnt == id of itme
        //dd( $this->crud);

        // update the row in the db
        // TODO save post && save images
        $item = $this->crud->model;
        //dd($this->crud->entry);
        $values = $this->crud->getStrippedSaveRequest();
        //dd( $values);
        foreach( $values as $key => $value )
            $item->$key = $value;
        $item = $this->crud->update($request->get($this->crud->model->getKeyName()), $values);
      //  $item->user_id = 1;
       // $item->save();

        // update entry with new value
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
        */
    }

    // private
    private function  _form()
    {
        CRUD::addField([
            'name' => 'title',
            'label' => 'Titre',
            'type' => 'text',
        ]);

        CRUD::addField([
            'name' => 'content',
            'label' => 'Contenu',
            'type' => 'tinymce',
        ]);

        CRUD::addField([
            'name' => 'photos',
            'label' => 'Images',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'path',
                    'type'    => 'image',
                    'label'   => 'Image',
                    'crop'    => 'true',
                    'aspect_ratio' => 1,
                    'wrapper' => ['class' => 'form-group col-md-12'],
                ],
                [
                    'name'    => 'label',
                    'type'    => 'text',
                    'label'   => 'Label',
                    'wrapper' => ['class' => 'form-group col-md-12'],
                ],
                [
                    'name'    => 'description',
                    'type'    => 'text',
                    'label'   => 'Description',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ],

            // optional
            'new_item_label'  => 'Ajouter image'
        ]);

        CRUD::addField([  // Select2
            'label'     => "Rédacteur",
            'type'      => 'select2',
            'name'      => 'user_id',

            // optional
            'entity'    => 'user',
            'model'     => "App\Models\User",
            'attribute' => 'name',
            'default'   => backpack_user()->id,
            'attributes' => [
                'disabled' => 'disabled'
            ],

            // also optional
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
        ]);
    }
}
