<?php


namespace App\Http\Controllers\Api;


use App\Facades\ActionManagerFacade as ActionManager;

class ActionApiController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function do($action_id)
    {
        $datas = ActionManager::run($action_id);

        if( $datas )
            if( count($datas) > 0 )
                return $this->sendSuccess($datas);
            else
                return $this->sendNoContent();
        else
            return $this->sendError('Error during action', 500);
    }
}
