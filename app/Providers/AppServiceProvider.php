<?php

namespace App\Providers;

use Backpack\PageManager\app\Models\Page;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\View\View;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // add pages to layout
        view()->composer('layouts.app', function ($view) {
            $plants = Page::where('template', 'plants')->get()->sortBy('title');
            $residents = Page::where('template', 'plants')->get()->sortBy('title');
            $services = Page::where('template', 'plants')->get()->sortBy('title');

            $view->with('plants', $plants );
            $view->with('residents', $residents );
            $view->with('services', $services );
        });

    }
}
