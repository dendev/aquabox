<?php
namespace App\Services;

use App\Libs\AquapyboxApi;
use App\Models\Action;
use Illuminate\Support\Facades\Log;

class ActionManagerService
{
    public function run($action_id)
    {
        $ok = false;

        $action = Action::find($action_id);

        if( $action )
            $ok = $this->ask_to_api($action->api_url);
        else
            \Log::error("[ActionManagerService:run] AMSr01: Ask unvalide action", [
                "action" => $action
            ]);

        return $ok;
    }

    public function get_actions_list()
    {
        $list = [];

        $actions = Action::all()->sortBy('device_id');
        foreach( $actions as $action)
        {
            $list[$action->id] = $action->label_full;
        }

        return $list;
    }

    public function ask_to_api($api_action_url)
    {
        $ok = false;

        $response = AquapyboxApi::do_action($api_action_url);

        if( $response && $response["success"])
        {
            $ok = $response;
        }

        $this->_log_slack($response, $api_action_url);

        return $ok;
    }

    private function _log_slack($response, $api_action_url)
    {
        if( $response )
        {
            $extras = [
                'action' => $response['infos']['action'],
                'default' => $response['infos']['default'],
                'description' => $response['infos']['description'],
                'resource' => $response['infos']['resource'],
            ];

            if ($response["success"])
                Log::channel('slack')->info($response['msg'], $extras);
            else
                Log::channel('slack')->error($response['msg'], $extras);
        }
        else
        {
            $extras = [
                'url' => $api_action_url
            ];
            Log::channel('slack')->error("No response received", $extras);
        }
    }
}
