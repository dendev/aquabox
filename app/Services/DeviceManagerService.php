<?php
namespace App\Services;

use App\Models\Action;
use App\Models\Device;
use Illuminate\Support\Facades\Log;

class DeviceManagerService
{

    public function create($datas, $base_url)
    {
        $ok = true;

        // device
        $image_url = (array_key_exists('image', $datas) && array_key_exists('url', $datas['image'])) ? $datas['image']['url'] : null;
        $image_description = (array_key_exists('image', $datas) && array_key_exists('description', $datas['image'])) ? $datas['image']['description'] : null;
        $device = Device::firstOrCreate(
            ['slug' => $datas['slug']],
            [
                'label' => $datas['label'],
                'description' => $datas['description'],
                'image_url' => $image_url,
                'image_description' => $image_description,
            ]
        );

        //  actions
        foreach ($datas['resources'] as $resource) {
            foreach ($resource['actions'] as $ra) {
                $api_url = $base_url . 'api/' . $resource['slug'] . '/' . $ra['slug'];
                $action = Action::firstOrCreate(
                    ['api_url' => $api_url],
                    [
                        'device_id' => $device->id,
                        'label' => $ra['label'],
                        'slug' => $ra['slug'],
                        'description' => $ra['description'],
                        'resource_label' => $resource['label'],
                        'resource_slug' => $resource['slug'],
                        'api_url' => $api_url
                    ]
                );
            }
        }

        return $ok;
    }
}
