# Aquabox

> Gestion aquarium 

# Install

```bash
composer install && npm install
cp .env.example .env && vim .env

php artisan passport:install 
php artisan key:generate
php artisan migrate --seed
```

Activer le scheduler de laravel [src](https://laravel.com/docs/7.x/scheduling#introduction)
```bash 
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```

S'assurer que le serveur est capable de résoudre les dns des aquapybox gérant les aquariums
